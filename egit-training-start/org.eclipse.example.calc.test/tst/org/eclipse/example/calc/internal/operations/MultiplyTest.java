package org.eclipse.example.calc.internal.operations;

import static org.junit.Assert.*;

import org.eclipse.example.calc.BinaryOperation;
import org.junit.Before;
import org.junit.Test;


public class MultiplyTest {

	private BinaryOperation op;

	@Before
	public void setUp() throws Exception {
		op = new Multiply();
	}

	@Test
	public void testPerform() {
		assertEquals(15.0, op.perform(5.0F, 3.0F), 0.01F);
	}
}
