package org.eclipse.example.calc.internal.operations;

import static org.junit.Assert.*;

import org.eclipse.example.calc.BinaryOperation;
import org.junit.Before;
import org.junit.Test;

public class DivideTest {

	private BinaryOperation op;

	@Before
	public void setUp() throws Exception {
		op = new Divide();
	}

	@Test
	public void testPerform() {
		assertEquals(5.0, op.perform(10.0F, 2.0F), 0.01F);
	}
}
