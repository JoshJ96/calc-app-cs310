package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.BinaryOperation;

public class Power extends AbstractOperation implements BinaryOperation  {
	@Override
	public float perform(float arg1, float arg2) {
		//Math.pow doesn't take in floats, so double conversions were needed
		return (float) Math.pow((double)arg1, (double)arg2);
	}

	@Override
	public String getName() {
		return "^";
	}
}
